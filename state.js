function State(gl, canvas, textures, defaultTexture, scene) {
    const hideSettingsButton     = document.getElementById('button-hide-settings'),
          hideSceneButton        = document.getElementById('button-hide-scene'),
          cameraPosText          = document.getElementById('camera-position'),
          pitchYawValuesText     = document.getElementById('pitch-yaw-values'),
          fpsInfoText            = document.getElementById('fps-info'),
          settingsButton         = document.getElementById('button-settings'),
          sceneButton            = document.getElementById('button-scene'),
          simButton              = document.getElementById('button-sim'),
          settingsPanel          = document.getElementById('settings-panel'),
          scenePanel             = document.getElementById('scene-panel'),
          sliderSimSpeed         = document.getElementById('slider-simspeed'),
          sliderSimSpeedValue    = document.getElementById('slider-simspeed-value'),
          sliderHFOV             = document.getElementById('slider-hfov'),
          sliderHFOVValue        = document.getElementById('slider-hfov-value'),
          sliderVFOV             = document.getElementById('slider-vfov'),
          sliderVFOVValue        = document.getElementById('slider-vfov-value'),
          sliderNear             = document.getElementById('slider-near'),
          sliderNearValue        = document.getElementById('slider-near-value'),
          sliderFar              = document.getElementById('slider-far'),
          sliderFarValue         = document.getElementById('slider-far-value');

    var hfov, vfov, near, far, projMat, simSpeed,
        camera = {pitch: 0.0, yaw: 0.0, pos: new Vector(0.0, 0.0, 3.0)},
        mouseXf = 0.01, mouseYf = 0.01, mouseMovementX = 0, mouseMovementY = 0,
        keys = {W: false, A: false, S: false, D: false}, moveSpeed = 0.005;

    var lastTime, lastFPSUpdate = 0, framesSinceLastFPSUpdate = 0;
    const FPS_UPDATE_INTERVAL_MS = 250;
    function frame(curTime) {
        resizeCanvas();
        gl.viewport(0, 0, canvas.width, canvas.height);

        if (lastTime === null) {
            lastTime = curTime;
        }

        var deltaTime = curTime - lastTime, deltaTimeSim = simSpeed * deltaTime;

        camera.pitch -= mouseXf * mouseMovementX;
        camera.yaw   = Math.max(Math.min(camera.yaw - mouseYf * mouseMovementY, Math.PI * 0.5), -Math.PI * 0.5);
        if (camera.pitch > 2 * Math.PI) {
            camera.pitch -= 2 * Math.PI;
        }
        if (camera.pitch < -2 * Math.PI) {
            camera.pitch += 2 * Math.PI;
        }
        mouseMovementX = mouseMovementY = 0;

        let delta = Vector.zero(), moveDelta = deltaTime * moveSpeed;
        if (keys.W || keys.S) {
            delta.add(new Vector(0.0, 0.0, keys.W ? -moveDelta : moveDelta));
        }
        if (keys.A || keys.D) {
            delta.add(new Vector(keys.D ? moveDelta : -moveDelta, 0.0, 0.0));
        }

        delta.matmul(Matrix.mul(Matrix.rotateY(camera.pitch), Matrix.rotateX(camera.yaw)));
        camera.pos.add(delta);

        cameraPosText.textContent = camera.pos.toString();
        pitchYawValuesText.textContent = `P: ${toDeg(camera.pitch)} Y: ${toDeg(camera.yaw)}`;
        if (curTime - lastFPSUpdate > FPS_UPDATE_INTERVAL_MS) {
            fpsInfoText.textContent = `FPS: ${(1000 / ((curTime - lastFPSUpdate) / framesSinceLastFPSUpdate)).toFixed(0)}`;
            lastFPSUpdate = curTime;
            framesSinceLastFPSUpdate = 0;
        }

        scene.frame(projMat, defaultTexture, Matrix.lookAt(camera.pos, camera.pitch, camera.yaw), deltaTimeSim);

        lastTime = curTime;
        ++framesSinceLastFPSUpdate;
        window.requestAnimationFrame(frame);
    }

    function setupCamera(cameraDesc) {
        if (!cameraDesc) {
            return;
        }

        camera.pitch = cameraDesc.pitch || 0;
        camera.yaw = cameraDesc.yaw || 0;

        if (cameraDesc.pos) {
            camera.pos = new Vector(cameraDesc.pos.x || 0, cameraDesc.pos.y || 0, cameraDesc.pos.z || 0);
        }
    }

    function updateMouse(e) {
        mouseMovementX += e.movementX;
        mouseMovementY += e.movementY;
    }

    function resizeCanvas() {
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
    }

    canvas.addEventListener('click', function () {
        canvas.requestPointerLock();
    });
    function updateKeysUp(e) {
        switch (e.code) {
            case 'KeyA':
                keys.A = false;
                break;
            case 'KeyD':
                keys.D = false;
                break;
            case 'KeyS':
                keys.S = false;
                break;
            case 'KeyW':
                keys.W = false;
                break;
        }
    }
    function updateKeysDown(e) {
        switch (e.code) {
            case 'KeyA':
                keys.A = true;
                break;
            case 'KeyD':
                keys.D = true;
                break;
            case 'KeyS':
                keys.S = true;
                break;
            case 'KeyW':
                keys.W = true;
                break;
        }
    }
    document.addEventListener('pointerlockchange', function () {
        if (document.pointerLockElement === canvas) {
            document.addEventListener('mousemove', updateMouse);
            document.addEventListener('keyup', updateKeysUp);
            document.addEventListener('keydown', updateKeysDown);
        } else {
            document.removeEventListener('mousemove', updateMouse);
            document.removeEventListener('keyup', updateKeysUp);
            document.removeEventListener('keydown', updateKeysDown);
        }
    });

    var state = {
        get hfov() {
            return hfov;
        },

        get vfov() {
            return vfov;
        },

        get simSpeed() {
            return simSpeed;
        },

        get nearClippingPlane() {
            return near;
        },

        get farClippingPlane() {
            return far;
        },

        set hfov(x) {
            hfov = x;
            projMat = Matrix.perspectiveFOV(hfov, vfov, near, far);
            if (sliderHFOV.value != x) {
                sliderHFOV.value = x;
            }
            sliderHFOVValue.textContent = x;
        },

        set vfov(x) {
            vfov = x;
            projMat = Matrix.perspectiveFOV(hfov, vfov, near, far);
            if (sliderVFOV.value != x) {
                sliderVFOV.value = x;
            }
            sliderVFOVValue.textContent = x;
        },

        set nearClippingPlane(x) {
            if (!far || x < far) {
                near = x;
            }
            projMat = Matrix.perspectiveFOV(hfov, vfov, near, far);
            if (sliderNear.value != near) {
                sliderNear.value = near;
            }
            sliderNearValue.textContent = near;
        },

        set farClippingPlane(x) {
            if (!near || x > near) {
                far = x;
            }
            projMat = Matrix.perspectiveFOV(hfov, vfov, near, far);
            if (sliderFar.value != far) {
                sliderFar.value = far;
            }
            sliderFarValue.textContent = far;
        },

        set simSpeed(x) {
            if (x == 0.0) {
                simSpeed = 0.0;
                simButton.textContent = 'Start Simulation';
            } else {
                if (!simSpeed) {
                    simButton.textContent = 'Stop Simulation';
                }
                simSpeed = x;
            }

            if (sliderSimSpeed.value != simSpeed) {
                sliderSimSpeed.value = simSpeed;
            }
            sliderSimSpeedValue.textContent = simSpeed;
        }
    };

    state.hfov = 100.0;
    state.vfov = 70.0;
    state.nearClippingPlane = 0.1;
    state.farClippingPlane = 13.0;
    state.simSpeed = 1.0;

    window.requestAnimationFrame(frame);

    hideSettingsButton.addEventListener('click', function () {
        settingsPanel.style.display = 'none';
        settingsButton.style.display = 'block';
    });
    hideSceneButton.addEventListener('click', function () {
        scenePanel.style.display = 'none';
        sceneButton.style.display = 'block';
    });
    settingsButton.addEventListener('click', function () {
        settingsPanel.style.display = 'block';
        settingsButton.style.display = 'none';
    });
    sceneButton.addEventListener('click', function () {
        scenePanel.style.display = 'block';
        sceneButton.style.display = 'none';
    });
    simButton.addEventListener('click', function () {
        state.simSpeed = state.simSpeed ? 0.0 : 1.0;
    });

    // slider listeners
    [[sliderSimSpeed,'simSpeed'], [sliderHFOV,'hfov'], [sliderVFOV,'vfov'],
     [sliderNear,'nearClippingPlane'], [sliderFar,'farClippingPlane']].forEach(
        (s) => s[0].addEventListener('input', (e) => state[s[1]] = e.srcElement.valueAsNumber));

    // Scene Panel Stuff
    const cubeList             = document.getElementById('cubelist'),
          addCubeName          = document.getElementById('add-cube-name'),
          addCubeButton        = document.getElementById('add-cube-button'),
          deleteCubeButton     = document.getElementById('delete-cube-button'),
//          duplicateCubeButton  = document.getElementById('duplicate-cube-button'),
          clearSelectionButton = document.getElementById('clear-selection-button'),
          restartButton        = document.getElementById('button-restart-scene'),
          cubeName             = document.getElementById('cube-name'),
          cubeCCW              = document.getElementById('cube-ccw-cull-face-direction');

    const motionObjByName = {
        trmotion: TrMotion,
        rotmotion: RotMotion,
        scalemotion: ScaleMotion
    }, cubeMotionByName = {
        trmotion: 'trMotion',
        rotmotion: 'rotMotion',
        scalemotion: 'scaleMotion'
    };

    var motionElements = {trmotion: {}, rotmotion: {}, scalemotion: {}};
    ['trmotion', 'rotmotion', 'scalemotion'].forEach(function (name) {
        motionElements[name].dropdown = document.getElementById('cube-' + name + '-dropdown');
        motionElements[name].dropdownContainer = document.getElementById('cube-' + name + '-container');
        motionElements[name].motionSettings = document.getElementById(name + '-settings');
        motionElements[name].vectorValuesContainer = document.getElementById('cube-' + name + '-vectorvalues');
        motionElements[name].dropdownButton = document.getElementById('cube-' + name + '-button');
    });

    var idCount = 0, curCube, curCubeId;

    function genDropdownMenu(menu, entries) {
        var ul = document.createElement('ul'), lis = [];

        function mkli(name) {
            var li = document.createElement('li');
            li.textContent = name;
            lis.push(li);
            ul.appendChild(li);
        }

        mkli('\xa0'); // :P
        for(let entry of entries) {
            mkli(entry);
        }
        menu.appendChild(ul);

        return lis;
    }

    function makeDropdownButtonListener(dropdown) {
        return function (e) {
            if (curCube) {
                if (dropdown.classList.contains('dropdown-menu-show')) {
                    dropdown.classList.remove('dropdown-menu-show');
                } else {
                    dropdown.classList.add('dropdown-menu-show');
                }
            }
        }
    }

    function addCube(name, autoselect = false) {
        var div = document.createElement('div'),
            idName = 'cube-' + idCount++;

        div.classList.add('cubelist-item');
        div.id = idName;
        div.textContent = name;
        cubelist.appendChild(div);
        var cube = scene.addCube(idName, name);

        div.addEventListener('click', function (e) {
            var findResult = scene.findCube(e.target.id);
            if (findResult.length != 1) {
                console.error('findResult.length != 1');
                return;
            }
            if (curCube) {
                clearSelection();
            }

            curCube = findResult[0];
            curCube.selected = true;
            curCubeId = e.target.id;
            cubeName.value = div.textContent;
            fillMotionDivs();
            fillTextureDiv();
            fillMiscellaneousSettings();
            div.classList.add('cubelist-item-selected');
        });

        if (autoselect) {
            div.click();
        }

        return cube;
    }

    function clearCubeList() {
        while (cubeList.hasChildNodes()) {
            cubeList.removeChild(cubeList.lastChild);
        }
    }

    // motion divs
    function clearMotionDiv(name) {
        var dropdown = motionElements[name].dropdown,
            dropdownButton = motionElements[name].dropdownButton,
            motionSettings = motionElements[name].motionSettings,
            vectorValuesContainer = motionElements[name].vectorValuesContainer;

        dropdown.classList.remove('dropdown-menu-show');
        dropdownButton.textContent = '';
        while (motionSettings.hasChildNodes()) {
            motionSettings.removeChild(motionSettings.lastChild);
        }
        while (vectorValuesContainer.hasChildNodes()) {
            vectorValuesContainer.removeChild(vectorValuesContainer.lastChild);
        }
    }

    function fillMotionDiv(name, motion) {
        var numberValues = motion.htmlElements(MotionParameterType.number),
            vectorValues = motion.htmlElements(MotionParameterType.vector),
            motionSettings = motionElements[name].motionSettings,
            vectorValuesContainer = motionElements[name].vectorValuesContainer,
            dropdownButton = motionElements[name].dropdownButton;

        if (numberValues) {
            for (let numElement of numberValues) {
                motionSettings.appendChild(numElement);
            }
        }
        if (vectorValues) {
            for (let vElement of vectorValues) {
                vectorValuesContainer.appendChild(vElement);
            }
        }

        dropdownButton.textContent = motion.name;
    }

    function fillMotionDivs() {
        if (curCube) {
            Object.getOwnPropertyNames(motionElements).forEach(function (name) {
                var motion = curCube[cubeMotionByName[name]];
                if (motion) {
                    fillMotionDiv(name, motion);
                }
            });
        }
    }

    function fillMiscellaneousSettings() {
        if (curCube) {
            cubeCCW.checked = curCube.isCCW;
        }
    }

//    function copyCube(source) {
//        let cube = addCube(`${source.displayName}${markAsCopy ? ' copy' : ''}`);
//    }


    Object.getOwnPropertyNames(motionElements).forEach(function (name) {
        var dropdown = motionElements[name].dropdown,
            dropdownButton = motionElements[name].dropdownButton;

        var lis = genDropdownMenu(dropdown, motionObjByName[name].keys());
        for (let li of lis) {
            li.addEventListener('click', function (e) {
                let cubeMotion = curCube[cubeMotionByName[name]], selectedMotionName = li.textContent;

                if (selectedMotionName == '\xa0') {
                    curCube[cubeMotionByName[name]] = null;
                    clearMotionDiv(name);
                } else {
                    if (cubeMotion && cubeMotion.name != selectedMotionName || !cubeMotion) {
                        cubeMotion = motionObjByName[name].get(selectedMotionName)();
                        curCube[cubeMotionByName[name]] = cubeMotion;
                    }

                    clearMotionDiv(name);
                    fillMotionDiv(name, cubeMotion);
                }
            });
        }

        dropdownButton.addEventListener('click', makeDropdownButtonListener(dropdown));
    });

    // texture div
    var textureButton = document.getElementById('cube-texture-button'),
        textureDropdown = document.getElementById('cube-texture-dropdown'),
        textureImage    = document.getElementById('cube-texture-image');

    const textureDropdownEntries = genDropdownMenu(textureDropdown, textures.keys());

    textureButton.addEventListener('click', makeDropdownButtonListener(textureDropdown));

    function clearTextureDiv() {
        textureImage.src = '';
        textureImage.classList.add('cube-texture-image-hide');
        textureButton.textContent = '';
        textureDropdown.classList.remove('dropdown-menu-show');
    }

    function fillTextureDiv() {
        if (curCube.texture) {
            textureImage.src = curCube.texture.url;
            textureImage.classList.remove('cube-texture-image-hide');
            textureButton.textContent = curCube.texture.name;
        }
    }

    for (let li of textureDropdownEntries) {
        li.addEventListener('click', function(e) {
            const selectedTextureName = li.textContent;

            if (selectedTextureName === '\xa0') {
                clearTextureDiv();
                curCube.texture = null;
            } else {
                if (!curCube.texture || curCube.texture.name !== selectedTextureName) {
                    clearTextureDiv();
                    curCube.texture = textures.get(selectedTextureName);
                    fillTextureDiv();
                } else {
                    textureDropdown.classList.remove('dropdown-menu-show');
                }
            }
        });
    }

    function clearEverything() {
        scene.clear();
        idCount = 0;
        clearSelection();
        clearCubeList();
    }
    function clearSelection() {
        if (curCube) {
            curCube.selected = false;
            document.getElementById(curCubeId).classList.remove('cubelist-item-selected');
        }
        curCube = null;
        curCubeId = null;
        clearGUIs();
    }
    function clearGUIs () {
        clearTextureDiv();
        addCubeName.value = '';
        cubeName.value = '';
        cubeCCW.checked = false;
        Object.getOwnPropertyNames(motionElements).forEach(name => clearMotionDiv(name));
    }

    function sanitizeCubeName(name) {
        var sanitizedName = name.replace(/[^\w]/g, '');
        return sanitizedName.length === 0 ? 'unnamed' : sanitizedName;
    }

    addCubeButton.addEventListener('click', function (e) {
        addCube(sanitizeCubeName(addCubeName.value), true);
        addCubeName.value = "";
    });

    cubeName.addEventListener('change', function (e) {
        if (curCube) {
            let sanitizedName = sanitizeCubeName(cubeName.value);
            document.getElementById(curCubeId).textContent = sanitizedName;
            cubeName.value = sanitizedName;
            curCube.displayName = sanitizedName;
        }
    });

    cubeCCW.addEventListener('change', function (e) {
        if (curCube) {
            curCube.isCCW = !curCube.isCCW;
        } else {
            cubeCCW.checked = false;
        }
    });

    deleteCubeButton.addEventListener('click', function (e) {
        if (curCube) {
            let cubeDiv = document.getElementById(curCube.name), prevCubeDiv = cubeDiv.previousElementSibling,
                nextCubeDiv = cubeDiv.nextElementSibling;

            scene.deleteCube(curCube.name);

            if (nextCubeDiv) {
                nextCubeDiv.click();
                cubeList.removeChild(cubeDiv);
            } else if (prevCubeDiv) {
                prevCubeDiv.click();
                cubeList.removeChild(cubeDiv);
            } else {
                clearEverything();
            }
        }
    });

//    duplicateCubeButton.addEventListener('click', function (e) {
//        if (curCube) {
//            copyCube(curCube);
//        }
//    });

    clearSelectionButton.addEventListener('click', function (e) {
        clearSelection();
    });

    restartButton.addEventListener('click', function (e) {
        for (let cube of scene) {
            ['trMotion', 'rotMotion', 'scaleMotion'].forEach(function (x) {
                if (cube[x]) {
                    cube[x].updateStep();
                }
            });
        }
    });

    function loadSceneFromJSON(json) {
        var parsedScene = JSON.parse(json), protoCubes = [], success = true;
        let cubes = parsedScene.version === 2 ? parsedScene.scene : parsedScene;
        try {
            for (let cube of cubes) {
                var protoCube = {};
                Object.getOwnPropertyNames(motionElements).forEach(function (name) {
                    const cubeMotionName = cubeMotionByName[name],
                          cubeMotion     = cube[cubeMotionName];

                    if (cubeMotion) {
                        let motionObj = motionObjByName[name].get(cubeMotion.name)(),
                            motionParams = [];

                        for (let param of cubeMotion.params) {
                            let newParam = new MotionParameter(param.type, param.name, null);
                            switch (param.type) {
                                case MotionParameterType.number:
                                    newParam.value = param.value;
                                    break;
                                case MotionParameterType.vector:
                                    newParam.value = parsedScene.version === 2 ?
                                        new Vector(param.value.x, param.value.y, param.value.z) :
                                        new Vector(param.value.data[0], param.value.data[1], param.value.data[2]);
                                    break;
                            }
                            motionParams.push(newParam);
                        }

                        motionObj.setParametersNamed(...motionParams);
                        protoCube[cubeMotionName] = motionObj;
                    }
                });

                protoCube.displayName = cube.displayName;
                protoCube.isCCW = cube.isCCW;
                protoCube.texture = cube.texture ? textures.get(cube.texture.name) : null;
                protoCubes.push(protoCube);
            }
        } catch(e) {
            success = false;
            alert('Bad scene JSON :(\n' + e);
        }

        if (success) {
            clearEverything();
            for (let protoCube of protoCubes) {
                let cube = addCube(protoCube.displayName);
                cube.trMotion = protoCube.trMotion;
                cube.rotMotion = protoCube.rotMotion;
                cube.scaleMotion = protoCube.scaleMotion;
                cube.texture = protoCube.texture;
                cube.isCCW = protoCube.isCCW;
            }
            if (parsedScene.version === 2) {
                setupCamera(parsedScene.camera)
            }
        }
    }

    // load default scene
    let defaultSceneJSON;
    (function () {
        const xhr = new XMLHttpRequest();

        xhr.open(
            'GET',
            'resources/defaultscene.json'
        );

        xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status >= 200 && xhr.status < 400) {
                defaultSceneJSON = xhr.responseText;
                loadSceneFromJSON(defaultSceneJSON);
            }
        }
        });

        xhr.send();
    })();

    (function () {
        const downloadAnchor = document.getElementById('anchor-download-scene'),
              downloadSceneButton = document.getElementById('button-download-scene'),
              loadSceneButton = document.getElementById('button-load-scene'),
              loadDefaultSceneButton = document.getElementById('button-load-default-scene'),
              clearSceneButton = document.getElementById('button-clear-scene'),
              loadSceneInput = document.getElementById('input-load-scene');

        var url;
        downloadSceneButton.addEventListener('click', function (e) {
            var blob = new Blob([JSON.stringify({ version: 2, scene, camera } , null, 4)], {type: 'application/json'});

            if (url) {
                URL.revokeObjectURL(url);
            }

            url = URL.createObjectURL(blob);
            downloadAnchor.href = url;
            downloadAnchor.click();
        });

        clearSceneButton.addEventListener('click', function (e) {
            clearEverything();
        });

        loadSceneButton.addEventListener('click', function (e) {
            loadSceneInput.style.display = loadSceneInput.style.display !== 'block' ? 'block' : 'none';
        });
        loadSceneInput.addEventListener('change', function (e) {
            if (loadSceneInput.files.length > 0) {
                var reader = new FileReader();
                reader.readAsText(loadSceneInput.files[0]);
                reader.onload = function (e) {
                    loadSceneFromJSON(e.target.result);
                    loadSceneInput.value = '';
                    loadSceneButton.click();
                }
            }
        });
        loadDefaultSceneButton.addEventListener('click', function (e) {
            if (defaultSceneJSON) {
                loadSceneFromJSON(defaultSceneJSON);
            }
        });
    })();

    return state;
}
