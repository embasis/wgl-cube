const fs = require('fs');
const process = require('process');
fs.readFile('defaultscene.json', 'utf8', function (err, data) {
    if (err) {
        console.log('can\'t read defaultscene.json!');
        process.exit();
    }
    try {
        let json = JSON.parse(data.toString());
        fs.writeFile('root/resources/defaultscene.json', JSON.stringify(json), function (err) {
            if (err) {
                console.log('failed to write minified defaultscene.json!');
            }
        });
    } catch (e) {
        console.log('failed to parse defaultscene.json!');
        process.exit();
    }
});
