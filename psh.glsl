#version 300 es

precision mediump float;

uniform bool selected;
const vec4 selectedColor = vec4(0.0, 0.0, 1.0, 0.2);
uniform sampler2D tex;
in vec2 out_uv;
out vec4 outColor;

void main() {
    vec4 t = texture(tex, out_uv);
    outColor = selected ? mix(t, selectedColor, selectedColor.a) : t;
    //outColor = vec4(0.0, 0.0, 1.0, 1.0);
}
