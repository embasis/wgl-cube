function Vector(x, y, z) {
    this.data = new Float32Array(3);
    this.data[0] = x;
    this.data[1] = y;
    this.data[2] = z;
}

Vector.zero = function () {
    return new Vector(0.0, 0.0, 0.0);
}

Vector.prototype = {
    get x() {
        return this.data[0];
    },

    get y() {
        return this.data[1];
    },

    get z() {
        return this.data[2];
    },

    set x(v) {
        this.data[0] = v;
    },

    set y(v) {
        this.data[1] = v;
    },

    set z(v) {
        this.data[2] = v;
    },

    zero: function () {
        this.data[0] = 0.0;
        this.data[1] = 0.0;
        this.data[2] = 0.0;
    },

    copy: function () {
        return new Vector(this.data[0], this.data[1], this.data[2]);
    },

    dotProduct: function (v) {
        return this.data[0] * v.data[0] + this.data[1] * v.data[1] + this.data[2] * v.data[2];
    },

    length: function () {
        return Math.sqrt(this.dotProduct(this));
    },

    flip: function () {
        this.data[0] = -this.data[0];
        this.data[1] = -this.data[1];
        this.data[2] = -this.data[2];
    },

    normalize: function () {
        var len = this.length();
        this.data[0] /= len;
        this.data[1] /= len;
        this.data[2] /= len;
    },

    add: function (v) {
        this.data[0] += v.data[0];
        this.data[1] += v.data[1];
        this.data[2] += v.data[2];

        return this;
    },

    matmul: function (m) {
        var x = this.data[0] * m[0] + this.data[1] * m[4] + this.data[2] * m[8] + m[12],
            y = this.data[0] * m[1] + this.data[1] * m[5] + this.data[2] * m[9] + m[13],
            z = this.data[0] * m[2] + this.data[1] * m[6] + this.data[2] * m[10] + m[14],
            w = m[3] + m[7] + m[11] + m[15];

        if (w != 1.0) {
            x /= w;
            y /= w;
            z /= w;
        }

        this.data[0] = x;
        this.data[1] = y;
        this.data[2] = z;

        return this;
    },

    subtract: function (v) {
        this.data[0] -= v.data[0]
        this.data[1] -= v.data[1]
        this.data[2] -= v.data[2]

        return this;
    },

    toString: function () {
        return "X: " + this.data[0] + " Y: " + this.data[1] + " Z: " + this.data[2];
    },

    toJSON: function () {
        return { x: this.data[0], y: this.data[1], z: this.data[2] }
    }
}

