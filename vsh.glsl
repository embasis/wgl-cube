#version 300 es

uniform mat4 modelViewProj;
in vec4 pos;
in vec2 uv;
out vec2 out_uv;

void main() {
    gl_Position = modelViewProj * pos;
    out_uv = uv;
}
