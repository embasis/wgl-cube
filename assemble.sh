#!/bin/sh

[ ! -d root ] && mkdir root

tmpfile=$(mktemp)
awkhtml=$(mktemp)
awkjs=$(mktemp)

for shader in *.glsl; do
    echo -n '<script type="text/plain" id="'"$(basename $shader .glsl)"'Src">' >> $tmpfile
    cat $shader >> $tmpfile
    echo '</script>' >> $tmpfile
done

tee >/dev/null $awkhtml <<EOF
{
    if (\$1 == "\$SHADERS\$")
    {
        system("cat \"$tmpfile\"")
    }
    else
    {
        split(\$0, a, "\$");
        if (length(a) == 3 && a[2] == "TEXTURES")
        {
            printf a[1]
            system("find ./root/resources/* -printf \"%f\\n\"")
            print a[3]
        }
        else
        {
            print \$0
        }
    }
}
EOF
awk -f $awkhtml index.html > index.tmp.html

tee >/dev/null $awkjs <<EOF
{
    if (\$1 == "\$MATH")
    {
        system("cat math.js")
    }
    else if (\$1 == "\$MATRIX")
    {
        system("cat matrix.js")
    }
    else if (\$1 == "\$VECTOR")
    {
        system("cat vector.js")
    }
    else if (\$1 == "\$MOTION")
    {
        system("cat motion.js")
    }
    else if (\$1 == "\$STATE")
    {
        system("cat state.js")
    }
    else
    {
        print \$0
    }
}
EOF
awk -f $awkjs webgl-cube.js > webgl-cube.tmp.js

minify index.tmp.html > root/index.html
minify webgl-cube.tmp.js > root/webgl-cube.js
minify styles.css > root/styles.css
node json-minify.js

rm $tmpfile
rm $awkjs
rm index.tmp.html
rm webgl-cube.tmp.js
