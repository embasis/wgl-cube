const MotionParameterType = {
    number: 1,
    vector: 2
};

function MotionParameter(type, name, value) {
    this.type = type;
    this.name = name;
    this._value = value;
}
MotionParameter.prototype = {
    get value() {
        return this._value;
    },

    set value(x) {
        this._value = x;
        if (this.gui && this.gui.update) {
            this.gui.update(x);
        }
    },

    copy: function () {
        return new MotionParameter(this.type, this.name, this.value);
    }
};

function Motion(name, motionFunc, ...parameters) {
    function GuiNumber(name, value, min, max, step, onInputChange) {
        var div = document.createElement('div'),
            span = document.createElement('span'),
            input = document.createElement('input');

        span.innerHTML = name + ":";
        span.classList.add('motion-setting-value-name');

        input.setAttribute('type', 'number');
        input.classList.add('motion-setting-value');
        if (min) {
            input.setAttribute('min', min);
        }
        if (max) {
            input.setAttribute('max', max);
        }
        input.setAttribute('step', step ? step : 0.05);
        if (value != null && value != undefined) {
            input.value = value;
        }

        div.appendChild(span);
        div.appendChild(input);

        this.rootElement = div;
        this.inputElement = input;
        this.onInputChange = onInputChange;
        
        var SELF = this;
        this.inputListener = function (e) {
            if (SELF.onInputChange) {
                SELF.onInputChange(e.srcElement.value);
            }
        }
    }
    GuiNumber.prototype = {
        enableListener: function () {
            this.inputElement.addEventListener('change', this.inputListener);
        },

        disableListener: function () {
            this.inputElement.removeEventListener('change', this.inputListener);
        },

        enableImmediateMode: function () {
            this.inputElement.removeEventListener('change', this.inputListener);
            this.inputElement.addEventListener('input', this.inputListener);
        },
        
        disableImmediateMode: function () {
            this.inputElement.removeEventListener('input', this.inputListener);
            this.inputElement.addEventListener('change', this.inputListener);
        },

        update: function (value) {
            var valStr = value.toString();
            if (this.inputElement.value !== valStr) {
                this.inputElement.value = valStr;
            }
        }
    };

    function GuiVector(name, value, onInputChange) {
        var outerDiv = document.createElement('div'),
            nameDiv = document.createElement('div'),
            containerDiv = document.createElement('div'),
            X = new GuiNumber('X'),
            Y = new GuiNumber('Y'),
            Z = new GuiNumber('Z');

        nameDiv.innerHTML = name;
        nameDiv.classList.add('motion-settings-vector-name');
        containerDiv.classList.add('motion-settings-vector-container');
        containerDiv.appendChild(X.rootElement);
        containerDiv.appendChild(Y.rootElement);
        containerDiv.appendChild(Z.rootElement);
        outerDiv.appendChild(nameDiv);
        outerDiv.appendChild(containerDiv);

        this.X = X;
        this.Y = Y;
        this.Z = Z;
        if (value) {
            this.X.update(value.x);
            this.Y.update(value.y);
            this.Z.update(value.z);
        }
        this.rootElement = outerDiv;
        this.onInputChange = onInputChange;

        var SELF = this;
        this.assignCallbacks = function () {
            function onInputChange(component) {
                return function (value) {
                    var v = Vector.zero();
                    v.x = SELF.X.inputElement.value;
                    v.y = SELF.Y.inputElement.value;
                    v.z = SELF.Z.inputElement.value;
                    SELF.onInputChange(v);
                }
            }
            if (!X.onInputChange) {
                X.onInputChange = onInputChange('x');
            }
            if (!Y.onInputChange) {
                Y.onInputChange = onInputChange('y');
            }
            if (!Z.onInputChange) {
                Z.onInputChange = onInputChange('z');
            }
        }
    }
    GuiVector.prototype = {
        enableListener: function () {
            this.assignCallbacks();
            this.X.enableListener();
            this.Y.enableListener();
            this.Z.enableListener();
        },

        disableListener: function () {
            this.X.disableListener();
            this.Y.disableListener();
            this.Z.disableListener();
        },

        enableImmediateMode: function () {
            this.assignCallbacks();
            this.X.enableImmediateMode();
            this.Y.enableImmediateMode();
            this.Z.enableImmediateMode();
        },
        
        disableImmediateMode: function () {
            this.X.disableImmediateMode();
            this.Y.disableImmediateMode();
            this.Z.disableImmediateMode();
        },

        update: function (value) {
            this.X.update(value.x);
            this.Y.update(value.y);
            this.Z.update(value.z);
            this.value = value;
        },

    };

    return function (...args) {
        var params = parameters.map(p => p.copy());
        for (let p of params) {
            if (p.value && p.value.copy) {
                p.value = p.value.copy();
            }
        }

        var motionObj = {
            setParametersNamed: function (...parameters) {
                for (let pNew of parameters) {
                    for (let p of params) {
                        if (p.name === pNew.name && p.type === pNew.type) {
                            p.value = pNew.value;
                        }
                    }
                }
                this.updateStep();
            },

            setParametersPositioned: function (...parameters) {
                parameters.forEach((x,i) => {params[i].value = x});
                this.updateStep();
            },

            updateStep: function () {
                this.step = motionFunc(...params.map(p => p.value));
            },

            htmlElements: function (type) {
                return params.filter(p => p.type == type).map(p => p.gui.rootElement);
            },

            toJSON: function () {
                return {name: this.name, params: params.map(p => ({name: p.name, type: p.type, value: p.value}))};
            },

            name: name
        };

        for (let p of params) {
            function onInputChange(value) {
                p._value = value;
                motionObj.updateStep();
            }
            switch (p.type) {
                case MotionParameterType.number:
                    p.gui = new GuiNumber(p.name, p.value, null, null, null, onInputChange);
                    break;
                case MotionParameterType.vector:
                    p.gui = new GuiVector(p.name, p.value, onInputChange);
                    break;
            }
        }
        //motionObj.setParametersPositioned(...args);
        motionObj.updateStep();
        params.forEach(p => p.gui.enableListener());

        return motionObj;
    };
}

var MotionFunctions = {
    axisRotation: function (axis, speed) {
        var angle = 0.0;
        speed *= 0.0001;
        axis.normalize();
        return function (deltaTime) {
            angle += speed * deltaTime;
            if (angle > 2.0 * Math.PI) {
                angle -= 2.0 * Math.PI;
            }
            return Matrix.rotate(axis, angle);
        }
    },

    translation: function (pos) {
        return function (deltaTime) {
            return Matrix.translation(pos);
        }
    },

    scale: function (sv) {
        return function (deltaTime) {
            return Matrix.scaling(sv);
        }
    },

    circular: function (center, start, radius, angleX, angleY, angleZ, angleSpeed) {
        const radCoeff = 2 * Math.PI / 360;
        var angle = 0.0;

        angleSpeed *= 0.001 * 2 * Math.PI;
        angleX *= radCoeff;
        angleY *= radCoeff;
        angleZ *= radCoeff;

        if (start > 1.0) {
            start = 1.0;
        }
        if (start < 0.0) {
            start = 0.0;
        }

        var radiusV = (new Vector(0.0, 0.0, -radius)).matmul(Matrix.rotateY(start * 2 * Math.PI));

        return function (deltaTime) {
            angle += deltaTime * angleSpeed;
            if (angle > 2 * Math.PI) {
                angle -= 2 * Math.PI;
            }
            return Matrix.translation(radiusV.copy().matmul(Matrix.rotateY(angle))
                   .matmul(Matrix.rotateX(angleX))
                   .matmul(Matrix.rotateY(angleY)).matmul(Matrix.rotateZ(angleZ)).add(center));
        }
    },

    oscillation: function (t, dMin, dMax) {
        const c = 2 * Math.PI / t;
        var curT = 0.0, d = dMax - dMin;
        return function (deltaTime) {
            curT += deltaTime * 0.0001;
            if (curT > 2*t) {
                curT -= 2*t;
            }
            return dMin + 0.5 * d * (1.0 - Math.cos(c * curT));
        }
    },

    oscillationV: function (tv, dMinv, dMaxv) {
        const x = MotionFunctions.oscillation(tv.x, dMinv.x, dMaxv.x),
              y = MotionFunctions.oscillation(tv.y, dMinv.y, dMaxv.y),
              z = MotionFunctions.oscillation(tv.z, dMinv.z, dMaxv.z);
        return function (deltaTime) {
            return new Vector(x(deltaTime), y(deltaTime), z(deltaTime));
        }
    },

    oscillationScale: function (tv, sMinv, sMaxv) {
        const oscillationV = MotionFunctions.oscillationV(tv, sMinv, sMaxv);
        return function (deltaTime) {
            return Matrix.scaling(oscillationV(deltaTime));
        }
    },

    oscillationPosition: function (tv, startV, endV) {
        const oscillationV = MotionFunctions.oscillationV(tv, startV, endV);
        return function (deltaTime) {
            return Matrix.translation(oscillationV(deltaTime));
        }
    }
};

function mkMotionMap (...elements) {
    return new Map(elements.map(x => [x[0], new Motion(x[0], ...x.slice(1))]));
}

var RotMotion = mkMotionMap(
    ['AxisRotation',
     MotionFunctions.axisRotation,
     new MotionParameter(MotionParameterType.vector, 'Axis', new Vector(0.0, 1.0, 0.0)),
     new MotionParameter(MotionParameterType.number, 'Speed', 1.0)]
);

var TrMotion = mkMotionMap(
    ['Translation',
     MotionFunctions.translation,
     new MotionParameter(MotionParameterType.vector, 'Position', new Vector(0.0, 0.0, 0.0))],

    ['Oscillation',
     MotionFunctions.oscillationPosition,
     new MotionParameter(MotionParameterType.vector, 'Time', new Vector(1.0, 1.0, 1.0)),
     new MotionParameter(MotionParameterType.vector, 'Start', Vector.zero()),
     new MotionParameter(MotionParameterType.vector, 'Finish', Vector.zero())],

    ['Circular',
     MotionFunctions.circular,
     new MotionParameter(MotionParameterType.vector, 'Center', Vector.zero()),
     new MotionParameter(MotionParameterType.number, 'Start', 0.0),
     new MotionParameter(MotionParameterType.number, 'Radius', 1.0),
     new MotionParameter(MotionParameterType.number, 'AngleX', 0.0),
     new MotionParameter(MotionParameterType.number, 'AngleY', 0.0),
     new MotionParameter(MotionParameterType.number, 'AngleZ', 0.0),
     new MotionParameter(MotionParameterType.number, 'Angle Speed', 1.0)]
);

var ScaleMotion = mkMotionMap(
    ['Scale',
     MotionFunctions.scale,
     new MotionParameter(MotionParameterType.vector, 'Scaling Coefficients', new Vector(1.0, 1.0, 1.0))],

    ['Oscillation',
     MotionFunctions.oscillationScale,
     new MotionParameter(MotionParameterType.vector, 'Time', new Vector(1.0, 1.0, 1.0)),
     new MotionParameter(MotionParameterType.vector, 'Start', new Vector(1.0, 1.0, 1.0)),
     new MotionParameter(MotionParameterType.vector, 'Finish', new Vector(1.0, 1.0, 1.0))]
);
