"use strict";
(function (){
    $MATH
    $MATRIX
    $VECTOR
    $MOTION
    $STATE

    function showInitError() {
        /*let errorHeading = document.body.appendChild('h1');
        document.body.insertBefore(document.createTextNode('NO WEBGL FOR YOU :(:(:( SO SORRY'));*/
        document.getElementById('webgl-initerr').style.display = 'block';
    }

    function Scene(gl, prog) {
        var cubeCnt = 0,
            cubesTail = null,
            cubesHead = null,
            vertBuf = gl.createBuffer(),
            vao = gl.createVertexArray(),
            modelViewProjLoc,
            selectedLoc;

        function CubeNode(cube) {
            this.cube = cube;
            this.next = null;
            this.prev = cubesTail;
            if (cubesTail) {
                cubesTail.next = this;
            }
        }

        function Cube(name, displayName, texture, trMotion, rotMotion, scaleMotion, isCCW) {
            this.name = name;
            this.displayName = displayName;
            this.trMotion = trMotion;
            this.rotMotion = rotMotion;
            this.scaleMotion = scaleMotion;
            this.texture = texture;
            this.isCCW = isCCW;
            this.selected = false;
        }
        Cube.prototype.toJSON = function () {
            return {displayName: this.displayName, trMotion: this.trMotion, rotMotion: this.rotMotion, scaleMotion: this.scaleMotion, texture: this.texture, isCCW: this.isCCW};
        }

        this.addCube = function (name, displayName, texture, trMotion, rotMotion, scaleMotion, isCCW) {
            var cube = new Cube(name, displayName, texture, trMotion, rotMotion, scaleMotion, isCCW);
            cubesTail = new CubeNode(cube);
            if (!cubesTail.prev) {
                cubesHead = cubesTail;
            }
            return cube;
        }

        this.deleteCube = function (cubeName) {
            for (let p = cubesTail; p != null; p = p.prev) {
                if (p.cube.name == cubeName) {
                    if (p.prev) {
                        p.prev.next = p.next;
                    } else {
                        cubesHead = p.next;
                    }
                    if (p.next) {
                        p.next.prev = p.prev;
                    } else {
                        cubesTail = p.prev;
                    }
                    break;
                }
            }
        }

        this.findCube = function (name) {
            var arr = [];
            for (let cube of this) {
                if (name == cube.name) {
                    arr.push(cube);
                }
            }
            return arr;
        }

        this[Symbol.iterator] = function* () {
            for (let p = cubesHead; p != null; p = p.next) {
                yield p.cube;
            }
        }

        this.frame = function (projMat, defaultTexture, camera, deltaTime) {
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            for (let cube of this) {
                var scale = cube.scaleMotion && cube.scaleMotion.step(deltaTime),
                    rot = cube.rotMotion && cube.rotMotion.step(deltaTime),
                    tr = cube.trMotion && cube.trMotion.step(deltaTime),
                    modelViewProj = Matrix.identity();

                if (scale) {
                    modelViewProj = scale;
                }
                if (rot) {
                    modelViewProj = Matrix.mul(rot, modelViewProj);
                }
                if (tr) {
                    modelViewProj = Matrix.mul(tr, modelViewProj);
                }
                modelViewProj = Matrix.mul(projMat, Matrix.mul(camera, modelViewProj));
                gl.uniformMatrix4fv(modelViewProjLoc, false, modelViewProj);
                gl.uniform1i(selectedLoc, cube.selected);

                gl.bindTexture(gl.TEXTURE_2D, cube.texture ? cube.texture.textureId : defaultTexture);
                gl.frontFace(cube.isCCW ? gl.CCW : gl.CW);
                gl.drawArrays(gl.TRIANGLES, 0, 36);
            }
        }

        this.clear = function () {
            cubesHead = cubesTail = null;
        }

        this.toJSON = function () {
            return [...this];
        }

        modelViewProjLoc = gl.getUniformLocation(prog, 'modelViewProj');
        if (modelViewProjLoc == -1) {
            console.log("modelViewProjLoc is not found!");
            return;
        }
        selectedLoc = gl.getUniformLocation(prog, 'selected');
        if (selectedLoc == -1) {
            console.log("selected is not found!");
            return;
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, vertBuf);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
            -0.5, -0.5,  0.5, 0.0, 1.0,
            -0.5,  0.5,  0.5, 0.0, 0.0,
             0.5,  0.5,  0.5, 1.0, 0.0,
            -0.5, -0.5,  0.5, 0.0, 1.0,
             0.5,  0.5,  0.5, 1.0, 0.0,
             0.5, -0.5,  0.5, 1.0, 1.0,

             0.5, -0.5,  0.5, 0.0, 1.0,
             0.5,  0.5,  0.5, 0.0, 0.0,
             0.5,  0.5, -0.5, 1.0, 0.0,
             0.5, -0.5,  0.5, 0.0, 1.0,
             0.5,  0.5, -0.5, 1.0, 0.0,
             0.5, -0.5, -0.5, 1.0, 1.0,

             0.5, -0.5, -0.5, 0.0, 1.0,
             0.5,  0.5, -0.5, 0.0, 0.0,
            -0.5,  0.5, -0.5, 1.0, 0.0,
             0.5, -0.5, -0.5, 0.0, 1.0,
            -0.5,  0.5, -0.5, 1.0, 0.0,
            -0.5, -0.5, -0.5, 1.0, 1.0,

            -0.5, -0.5, -0.5, 0.0, 1.0,
            -0.5,  0.5, -0.5, 0.0, 0.0,
            -0.5,  0.5,  0.5, 1.0, 0.0,
            -0.5, -0.5, -0.5, 0.0, 1.0,
            -0.5,  0.5,  0.5, 1.0, 0.0,
            -0.5, -0.5,  0.5, 1.0, 1.0,

            -0.5,  0.5,  0.5, 0.0, 1.0,
            -0.5,  0.5, -0.5, 0.0, 0.0,
             0.5,  0.5, -0.5, 1.0, 0.0,
            -0.5,  0.5,  0.5, 0.0, 1.0,
             0.5,  0.5, -0.5, 1.0, 0.0,
             0.5,  0.5,  0.5, 1.0, 1.0,

             0.5, -0.5,  0.5, 0.0, 1.0,
             0.5, -0.5, -0.5, 0.0, 0.0,
            -0.5, -0.5, -0.5, 1.0, 0.0,
             0.5, -0.5,  0.5, 0.0, 1.0,
            -0.5, -0.5, -0.5, 1.0, 0.0,
            -0.5, -0.5,  0.5, 1.0, 1.0]), gl.STATIC_DRAW);
        gl.bindVertexArray(vao);
        var posLoc = gl.getAttribLocation(prog, 'pos');
        gl.enableVertexAttribArray(posLoc);
        gl.vertexAttribPointer(posLoc, 3, gl.FLOAT, false, 20, 0);
        var uvLoc = gl.getAttribLocation(prog, 'uv');
        gl.enableVertexAttribArray(uvLoc);
        gl.vertexAttribPointer(uvLoc, 2, gl.FLOAT, false, 20, 12);
    }

    function createShader(gl, type, src) {
        var sh = gl.createShader(type);
        gl.shaderSource(sh, src);
        gl.compileShader(sh);
        if (!gl.getShaderParameter(sh, gl.COMPILE_STATUS)) {
            console.log(gl.getShaderInfoLog(sh));
            gl.deleteShader(sh);
            return null;
        }
        return sh;
    }

    function createProgram(gl, vsh, psh) {
        var prog = gl.createProgram();
        gl.attachShader(prog, vsh);
        gl.attachShader(prog, psh);
        gl.linkProgram(prog);
        if (!gl.getProgramParameter(prog, gl.LINK_STATUS)) {
            console.log(gl.getProgramInfoLog(prog));
            gl.deleteProgram(prog);
            return null;
        }
        return prog;
    }

    function makeDefaultTexture(gl) {
        var tex = gl.createTexture();

        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));

        return tex;
    }

    function Texture(gl, name, url) {
        var img = new Image(), tex = gl.createTexture();
        img.src = url;

        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));

        img.addEventListener('load', function () {
            gl.bindTexture(gl.TEXTURE_2D, tex);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
            gl.generateMipmap(gl.TEXTURE_2D);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        });

        this.name = name;
        this.url = url;
        this.textureId = tex;
    }
    Texture.prototype.toJSON = function () {
        return {name: this.name};
    }

    function loadTextures(gl) {
        return new Map(document.getElementById('script-texture-list').innerHTML.split('\n')
               .filter(s => s.length > 0)
               .map(name => [name, new Texture(gl, name, 'resources/' + name)]));
    }

    var state;

    function main() {
        var canvas        = document.getElementById('canvas'),
            gl            = canvas.getContext('webgl2');

        if (!gl) {
            console.log('getContext webgl2 failed! :(');
            showInitError();
            return;
        }

        var vsh = createShader(gl, gl.VERTEX_SHADER, document.getElementById('vshSrc').innerHTML),
            psh = createShader(gl, gl.FRAGMENT_SHADER, document.getElementById('pshSrc').innerHTML);

        if (!vsh || !psh) {
            console.log('createShader failed!');
            showInitError();
            return;
        }

        var prog = createProgram(gl, vsh, psh);

        if (!prog) {
            console.log('createProgram failed!');
            showInitError();
            return;
        }
        
        var scene = new Scene(gl, prog),
            textures = loadTextures(gl),
            defaultTexture = makeDefaultTexture(gl);

        state = State(gl, canvas, textures, defaultTexture, scene),

        gl.clearColor(0, 0, 0, 1);
        gl.useProgram(prog);
        gl.enable(gl.CULL_FACE);
        gl.frontFace(gl.CW);
        gl.enable(gl.DEPTH_TEST);
    }

    main();
})();
