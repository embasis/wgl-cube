var Matrix = {
    identity: function () {
        var m = new Float32Array(16);
        m[0] = m[5] = m[10] = m[15] = 1.0;
        return m;
    },

    translation: function (dv) {
        var m = this.identity();
        m[12] = dv.x;
        m[13] = dv.y;
        m[14] = dv.z;
        return m;
    },

    scaling: function (dv) {
        var m = this.identity();
        m[0] = dv.x;
        m[5] = dv.y;
        m[10] = dv.z;
        return m;
    },

    rotateX: function (a) {
        var m = this.identity(), s = Math.sin(a), c = Math.cos(a);
        m[5] = c;
        m[6] = s;
        m[9] = -s;
        m[10] = c;
        return m;
    },

    rotateY: function (a) {
        var m = this.identity(), s = Math.sin(a), c = Math.cos(a);
        m[0] = c;
        m[2] = -s;
        m[8] = s;
        m[10] = c;
        return m;
    },

    rotateZ: function (a) {
        var m = this.identity(), s = Math.sin(a), c = Math.cos(a);
        m[0] = c;
        m[1] = s;
        m[4] = -s;
        m[5] = c;
        return m;
    },

    rotate: function (u, angle) {
        var m = new Float32Array(16), s = Math.sin(angle), c = Math.cos(angle), t = 1 - c;
        m[0] = c + u.x*u.x*t
        m[1] = u.y*u.x*t + u.z*s;
        m[2] = u.z*u.x*t - u.y*s;
        m[4] = u.x*u.y*t - u.z*s;
        m[5] = c + u.y*u.y*t;
        m[6] = u.z*u.y*t + u.x*s;
        m[8] = u.x*u.z*t + u.y*s;
        m[9] = u.y*u.z*t - u.x*s;
        m[10] = c + u.z*u.z*t;
        m[15] = 1.0;
        return m;
    },

    perspective: function (l, r, b, t, n, f) {
        var m = new Float32Array(16);
        m[0] = 2*n/(r-l);
        m[5] = 2*n/(t-b);
        m[8] = (r+l)/(r-l);
        m[9] = (t+b)/(t-b);
        m[10] = -(f+n)/(f-n);
        m[11] = -1.0;
        m[14] = -2*f*n/(f-n);
        return m;
    },

    perspectiveFOV: function (hfov, vfov, n, f) {
        var hrad = Math.PI * hfov / 180.0,
            vrad = Math.PI * vfov / 180.0,
            r = n * Math.tan(0.5 * hrad), l = -r,
            t = n * Math.tan(0.5 * vrad), b = -t;
        return this.perspective(l, r, b, t, n, f);
    },

    lookAt: function (pos, pitch, yaw) {
        var posInv = pos.copy();
        posInv.flip();
        return this.mul(this.rotateX(-yaw), this.mul(this.rotateY(-pitch), this.translation(posInv)));
    },

    mul: function (a, b) {
        var c = new Float32Array(16);
        for (let i = 0; i < 4; ++i) {
            for (let j = 0; j < 4; ++j) {
                for (let k = 0; k < 4; ++k) {
                    c[i + 4*j] += a[i + 4*k] * b[4*j + k];
                }
            }
        }
        return c;
    }
};
